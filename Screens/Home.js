import React from 'react'
import { StyleSheet, View, ScrollView,TouchableOpacity,Text } from 'react-native'
const Home = ({ navigation }) => {
  return (
    <ScrollView style={styles.Contanier} vertical={true}>
      <View style={styles.ScrollView_Screen}>
        <View style={styles.btn_Button}>
          <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('uploadFiles')}>
                <Text style={{ fontSize: 20, color: '#fff' }}>Upload Files!</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('localNotification')}>
                <Text style={{ fontSize: 20, color: '#fff' }}>Notification</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('YoutubePlayer')}>
                <Text style={{ fontSize: 20, color: '#fff' }}>Youtube Video Player</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('testScreen')}>
                <Text style={{ fontSize: 20, color: '#fff' }}>Test Screen</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.btn} onPress={() => navigation.navigate('OTPScreen')}>
                <Text style={{ fontSize: 20, color: '#fff' }}>OTP Screen</Text>
            </TouchableOpacity>
            
        </View>
      </View>
    </ScrollView>
  )
}

export default Home

const styles = StyleSheet.create({
  Contanier: {
    flex: 1,
    marginTop: 10,
    
    marginBottom: 8
  },
  btn:{
    borderRadius:12,
    backgroundColor: '#1493e3',
    width: '60%', 
    justifyContent: 'flex-end', 
    alignItems: 'center',
    alignSelf:'center',
    padding:12,
    margin:20
},
  ScrollView_Screen: {
    marginTop: 12
  }
})
