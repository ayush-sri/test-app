import React, { useState, useEffect } from "react";
import {
  View,
  Text,
  Image,
  StyleSheet,
  Alert,
  Linking,
  FlatList,
  TouchableOpacity,
  ScrollView,
  ActivityIndicator,
} from "react-native";
import Button from "../Components/Button";
import { MaterialCommunityIcons, Entypo } from "@expo/vector-icons";
import * as DocumentPicker from "expo-document-picker";
import * as ImagePicker from "expo-image-picker";
import * as Application from "expo-application";
import * as IntentLauncher from "expo-intent-launcher";
import { Video } from "expo-av";
import FlashMessage from "react-native-flash-message";
import { showMessage } from "react-native-flash-message";
import ActionSheet from "react-native-custom-actionsheet";

const CANCEL_INDEX = 0;
const DESTRUCTIVE_INDEX = 4;
const options = ["Cancel", "Photos", "Documents"];
const title = "Please Choose from the following options";

const showActionSheet = () => actionSheet.show();
const getActionSheetRef = (ref) => (actionSheet = ref);

const uploadFiles = ({ navigation }) => {
  const [taskItems, setTaskItems] = useState();
  const [loading, setLoading] = useState();
  const [displayItems, setDisplayItems] = useState([]);
  const [countIndex, setCountIndex] = useState(0);

  const open_settings = () => {
    if (Platform.OS === "ios") {
      Linking.openURL(`app-settings:`);
    } else {
      IntentLauncher.startActivityAsync(
        IntentLauncher.ACTION_APPLICATION_DETAILS_SETTINGS,
        {
          data: `package:${Application.applicationId}`,
        }
      );
    }
  };
  const createAlert = () =>
    Alert.alert(
      "Permission error !",
      "Please grant media access permission from app settings",
      [
        {
          text: "Cancel",
        },
        { text: "OK", onPress: () => open_settings() },
      ],
      { cancelable: false }
    );
  let selectFiles = async () => {
    let permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      createAlert();
    } else {
      let result = await DocumentPicker.getDocumentAsync({ multiple: true });
      let filename = result.name;
      if (!result.cancelled) {
        // state update for displaying thumbnail on screen
        setDisplayItems([
          ...displayItems,
          {
            id: countIndex,
            uri: result.uri,
            type: "others",
          },
        ]);
        setCountIndex(countIndex + 1);
        //api call for upload
        imageSave(result.uri, "documents", filename);
      }
    }
  };

  let selectImage = async () => {
    let permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      createAlert();
    } else {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Images,
        allowsEditing: true,
        quality: 1,
      });
      let filename = result.uri.split("/").pop();
      if (!result.cancelled) {
        // state update for displaying thumbnail on screen
        setDisplayItems([
          ...displayItems,
          {
            id: countIndex,
            uri: result.uri,
            type: "img",
          },
        ]);
        setCountIndex(countIndex + 1);
        //api call for upload
        imageSave(result.uri, result.type, filename);
      }
    }
  };

  let selectVideo = async () => {
    let permissionResult =
      await ImagePicker.requestMediaLibraryPermissionsAsync();

    if (permissionResult.granted === false) {
      createAlert();
    } else {
      let result = await ImagePicker.launchImageLibraryAsync({
        mediaTypes: ImagePicker.MediaTypeOptions.Videos,
        allowsEditing: true,
        videoMaxDuration: 30,
      });
      let filename = result.uri.split("/").pop();
      if (!result.cancelled) {
        // state update for displaying thumbnail on screen
        setDisplayItems([
          ...displayItems,
          {
            id: countIndex,
            uri: result.uri,
            type: "img",
          },
        ]);
        setCountIndex(countIndex + 1);

        //api call for upload
        imageSave(result.uri, result.type, filename);
      }
    }
  };
  const handlePress = (index) => {
    if (index === 1) {
      selectImage();
    } else if (index === 2) {
      selectFiles();
    }
  };

  const imageSave = (imageURI, rtype, filename) => {
    try {
      let localUri = imageURI;
      //console.log('localUri', localUri);
      // let filename = localUri.split('/').pop();
      //console.log('filename is hereee',filename)

      let match = /\.(\w+)$/.exec(filename);
      //console.log('match',match)

      let type = match ? `${rtype}/${match[1]}` : `${rtype}`;
      //console.log('type',type)
      //console.log(type)
      const profile_pic = {
        name: filename,
        type: type,
        uri: localUri,
      };
      //console.log('profile_pic',profile_pic)
      //console.log(profile_pic)
      //console.log(props.groupId)
      const formData = new FormData();

      formData.append("identifier", "162442856388");
      formData.append("upload_type", "portfolio");
      formData.append("file_uploads", profile_pic);

      //console.log(formData);
      setLoading(true);

      fetch("https://www.rozgaarindia.com/bridge/putFileUploads", {
        method: "POST",
        timeout: 10000,
        headers: {
          Accept: "application/json",
          "Content-Type": "multipart/form-data",
          "Auth-token": "QtLRxjAC4ulU19iyX0FzxU6VB5Wwm8cBbtlVkxsF",
        },
        body: formData,
      })
        .then((response) => response.json())
        //.then((json) => console.log(json))
        //.then ((responseData) => console.log (responseData))
        //.then((result) => setUploadResponse(result))
        .then((json) => {
          if (json.status === "success") {
            setTaskItems(json.data);
            showMessage({
              message: "Your file has been uploaded sucessfully",

              type: "success",
              icon: "success",
              duration: 3500,
            });
          } else if (json.data === false) {
            showMessage({
              message: "Please check your internet connection",
              description:
                "Please try again. If the problem persists contact support",
              type: "danger",
              icon: "danger",
              duration: 3500,
            });
          }
        })
        .finally(() => setLoading(false))
        .catch((error) => console.error(error));
    } catch (error) {
      console.log("error----------------", error);
    }
  };

  const DeleteItem = (index) => {
    const removeIndex = displayItems.findIndex((item) => item.id == index);

    let temp = [...displayItems];
    temp.splice(removeIndex, 1);
    setDisplayItems([...temp]);
  };

  const FlatlistRender = (index, pathURI, type) => {
    switch (type) {
      case "img":
        return (
          <View style={styles.itemContainer}>
            {loading && displayItems.length == index + 1 ? (
              <ActivityIndicator
                size="large"
                color="#1493e3"
                style={styles.activityIndicator}
              />
            ) : (
              <View>
                <Image source={{ uri: pathURI }} style={styles.items} />
                <TouchableOpacity
                  onPress={() => {
                    DeleteItem(index);
                  }}
                  style={styles.crossBtn}
                >
                  <Entypo name="circle-with-cross" size={20} color="black" />
                </TouchableOpacity>
              </View>
            )}
          </View>
        );
        break;
      case "others":
        return (
          <View style={styles.itemContainer}>
            {loading && displayItems.length == index + 1 ? (
              <ActivityIndicator
                size="large"
                color="#1493e3"
                style={styles.activityIndicator}
              />
            ) : (
              <View>
                <MaterialCommunityIcons
                  name="file-document"
                  size={100}
                  color="#9a9a9a"
                  style={styles.items}
                />

                <TouchableOpacity
                  onPress={() => {
                    DeleteItem(index);
                  }}
                  style={styles.crossBtn}
                >
                  <Entypo name="circle-with-cross" size={20} color="black" />
                </TouchableOpacity>
              </View>
            )}
          </View>
        );
        break;
      case "av":
        return (
          <View style={styles.itemContainer}>
            {loading && displayItems.length == index + 1 ? (
              <ActivityIndicator
                size="large"
                color="#1493e3"
                style={styles.activityIndicator}
              />
            ) : (
              <View>
                <Video
                  source={{ uri: item.portfoli_img_url_thumb }}
                  isPlaying={false}
                  resizeMode={"contain"}
                  useNativeControls
                  style={{ width: 50, height: 50, borderRadius: 5 }}
                />

                <TouchableOpacity
                  onPress={() => {
                    DeleteItem(index);
                  }}
                  style={styles.crossBtn}
                >
                  <Entypo name="circle-with-cross" size={20} color="black" />
                </TouchableOpacity>
              </View>
            )}
          </View>
        );
        break;
      default:
        break;
    }
  };

  return (
    <View style={styles.mainContainer}>
      <View style={{ flexDirection: "row" }}>
        <View style={styles.topProgressBarLeft} />
        <View style={styles.topProgressBarRight} />
      </View>
      <ScrollView>
        <Text style={styles.titleText}>Upload Work Samples</Text>
        <Text style={styles.titleDisc}>
          Showcase your previous work samples to get more orders
        </Text>
        <Button text="Upload files" onPress={showActionSheet} />

        <Text style={styles.greyText}>You can upload files upto 2 GB</Text>
        <Text style={styles.blackText}>Or</Text>
        <Text style={styles.blackText}>
          Upload your sample videos to attract more buyers
        </Text>
        <Button text="Upload Video" onPress={selectVideo} />
        <Text style={styles.greyText}>Max video length should be 30 secs</Text>

        <FlatList
          data={displayItems}
          contentContainerStyle={styles.itemsWrapper}
          horizontal={false}
          numColumns={3}
          scrollEnabled={false}
          keyExtractor={(item) => item.id}
          renderItem={({ item, index }) => (
            <View>{FlatlistRender(item.id, item.uri, item.type)}</View>
          )}
        />
      </ScrollView>
      <TouchableOpacity
        style={styles.nextBtn}
        onPress={() => navigation.navigate("selectCover")}
      >
        <Text style={{ fontSize: 20, color: "#fff" }}>Next</Text>
      </TouchableOpacity>
      <ActionSheet
        ref={getActionSheetRef}
        title={title}
        message=" Do not share your personal details, it is against the policy of Rozgaar India to share any personal infomation "
        options={options}
        cancelButtonIndex={CANCEL_INDEX}
        destructiveButtonIndex={DESTRUCTIVE_INDEX}
        onPress={handlePress}
      />
      <FlashMessage position="top" />
    </View>
  );
};

export default uploadFiles;
const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: "#FFFFFF",
  },
  topProgressBarLeft: {
    width: "80%",
    borderWidth: 5,
    backgroundColor: "#007f00",
    borderColor: "#007f00",
    margin: 0,
  },
  topProgressBarRight: {
    width: "20%",
    borderWidth: 5,
    backgroundColor: "#54e599",
    borderColor: "#54e599",
    margin: 0,
  },
  titleText: {
    paddingTop: 15,
    paddingStart: 20,
    fontSize: 20,
    fontWeight: "bold",
  },
  titleDisc: {
    paddingStart: 20,
    paddingEnd: 20,
    paddingBottom: 30,
    color: "#9a9a9a",
    fontSize: 16,
    fontWeight: "bold",
  },
  greyText: {
    textAlign: "center",
    paddingTop: 10,
    paddingBottom: 10,
    color: "#9a9a9a",
    fontSize: 13,
    fontWeight: "bold",
  },
  blackText: {
    textAlign: "center",
    alignSelf: "center",
    justifyContent: "center",
    alignItems: "center",
    textAlignVertical: "center",
    alignContent: "center",
    paddingBottom: 15,
    paddingTop: 15,
    fontSize: 15,
    width: "60%",
  },
  activityIndicator: {
    borderRadius: 7,
    flex: 1,
    backgroundColor: "#9a9a9a",
  },
  itemsWrapper: {
    paddingBottom: 50,
    alignSelf: "center",
  },
  itemContainer: {
    flexDirection: "column",
    margin: 8,
    width: 110,
    alignSelf: "center",
    height: 110,
    borderRadius: 7,
  },
  items: {
    position: "absolute",
    marginTop: 10,
    width: 100,
    height: 100,
    borderRadius: 7,
  },
  crossBtn: {
    alignSelf: "flex-end",
  },
  uploadAnim: {
    // justifyContent: 'center',
    alignSelf: "center",
    height: 18,
    width: 144,
  },
  nextBtn: {
    borderRadius: 5,
    backgroundColor: "#007F00",
    width: "80%",
    justifyContent: "flex-end",
    alignItems: "center",
    alignSelf: "center",
    marginBottom: 10,
    padding: 12,
  },
});
