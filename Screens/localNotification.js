import React, { useEffect } from "react";
import { StyleSheet, Text, Alert, Linking,TouchableOpacity, View } from "react-native";
import * as Notifications from "expo-notifications";
import * as Application from 'expo-application';
import * as IntentLauncher from 'expo-intent-launcher';

const localNotification = () => {
    
  let count = 0;

  const open_settings = () => {
    if (Platform.OS === "ios") {
      Linking.openURL(`app-settings:`);
    } else {
      IntentLauncher.startActivityAsync(
        IntentLauncher.ACTION_APPLICATION_DETAILS_SETTINGS,
        {
          data: `package:${Application.applicationId}`,
        }
      );
    }
  };

  const createAlert = () =>
    Alert.alert(
      "Permission error !",
      "Please grant notification permission from app settings",
      [
        {
          text: "Cancel",
        },
        { text: "OK", onPress: () => open_settings() },
      ],
      { cancelable: false }
    );

  const sendLocalNotification = async () => {
    const permissionResult = await Notifications.requestPermissionsAsync();
    if (permissionResult.granted === false) {
      createAlert();
    }
    Notifications.setNotificationHandler({
      handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: true,
      }),
    });
    Notifications.scheduleNotificationAsync({
      content: {
        title: "Dummy Notification!",
        body: "This is just for testing",
      },
      trigger: null,
    });
    Notifications.setBadgeCountAsync(++count);
  };

  const resetBadge = () => {
    Notifications.setNotificationHandler({
      handleNotification: async () => ({
        shouldShowAlert: true,
        shouldPlaySound: true,
        shouldSetBadge: true,
      }),
    });
    count = 0;
    Notifications.setBadgeCountAsync(0);
  };

  return (
    <View style={styles.container}>
      <TouchableOpacity style={styles.Btn} onPress={sendLocalNotification}>
        <Text style={styles.btnText}>Test notification</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={resetBadge}>
        <Text style={styles.resetText}>Click to reset</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  Btn: {
    borderRadius: 5,
    backgroundColor: "#007F00",
    width: "60%",
    alignItems: "center",
    alignSelf: "center",
    marginTop: 20,
    padding: 12,
  },
  btnText: {
    fontSize: 20,
    color: "#fff",
  },
  resetText: {
    alignItems: "center",
    alignSelf: "center",
    paddingTop: 10,
    fontSize: 20,
  },
});
export default localNotification;
