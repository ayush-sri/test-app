import React, { useState, useEffect, useRef } from "react";
import {
  StyleSheet,
  Text,
  View,
  SafeAreaView,
  TouchableWithoutFeedback,
  Keyboard,
  Image,
  TouchableOpacity,
  TextInput,
} from "react-native";
import FlashMessage from "react-native-flash-message";
import { showMessage } from "react-native-flash-message";
import * as Clipboard from "expo-clipboard";
import OtpTimer from "../Components/OtpTimer";

const OTPScreen = () => {
  let textInput = useRef(null);
  const lengthInput = 4;
  const MyPlatform = Platform.OS === 'ios' ? "BuyerIOS" : "BuyerAndroid";
  const mobile = "7007030012";
  
  const [otpInput, setOtpInput] = useState("");
  const [myData, setMyData] = useState();
  const [refresh, setRefresh] = useState(false)
  

  const copyToClipboard = (value) => {
    Clipboard.setString(value);
  };

  const fetchCopiedText = async () => {
    const text = await Clipboard.getStringAsync();
    // setCopiedText(text);
    console.log(text);
  };

  useEffect(() => {
    textInput.focus();
    // try {
    // 	fetch('https://www.rozgaarindia.com/bridge/send_otp', {
    // 		method: 'POST',
    // 		timeout: 10000,
    // 		headers: {
    // 			Accept: 'application/json',
    // 			'Content-Type': 'application/json',
    // 			'Auth-Token': 'QtLRxjAC4ulU19iyX0FzxU6VB5Wwm8cBbtlVkxsF'
    // 		},
    // 		body: JSON.stringify({
    // 			"mobile": (mobile),
    // 		})
    // 	})
    // 		.then((response) => response.json())
    //     .then((json) => console.log(json))
    //   //   .then((json) => {
    //   //     if (json.status === 'success') {
    
    //   //     }
    //   // })
    // 		.catch((error) => ApiCatch({ error: error, source: 'OtpScreen - send_otp' }))
    // } catch (error) {
    // 	ApiCatch({ error: error, source: 'OtpScreen - send_otp' })
    // }
  }, []);

  const onChangeText = (val) => {
    setOtpInput(val);
  };

  const callApi = (code) => {
		try {
			fetch('https://www.rozgaarindia.com/bridge/signup', {
				method: 'POST',
				timeout: 10000,
				headers: {
					Accept: 'application/json',
					'Content-Type': 'application/json',
					'Auth-Token': 'QtLRxjAC4ulU19iyX0FzxU6VB5Wwm8cBbtlVkxsF'
				},
				body: JSON.stringify({
					"mobile": (mobile),
					"otp": (code),
					"device_id": "react-app",
					"reg_type": "buyer",
					"logintype": "buyer",
					"Guid": "",
					"utm_source": "",
					"adsTrackID": (MyPlatform),
					"request_type": (MyPlatform)
				})
			})
				.then((response) => response.json())
				.then((result) => {
					if (result.status === "SUCCESS") {
						saveDataToStorage(result.data.user_token, result.data.token, result.data.username, result.data.USERID, result.data.gold_member);
						login();
					} else {
						showMessage({
							message: "OTP is incorrect",
							description: "Please enter a valid OTP",
							type: 'danger',
							icon: "danger",
							duration: 3000
						});
					};
				})
				.catch((error) => ApiCatch({ error: error, source: 'OtpScreen - signup' }))
		} catch (error) {
			ApiCatch({ error: error, source: 'OtpScreen - signup' })
		}
	}


  const OtpStatus = () => {
    if (myData === "fail") {
      showMessage({
        message: "Please check your internet",
        description: "There is some issue is sending OTP",
        type: "danger",
        icon: "danger",
        duration: 3000,
      });
    } else {
      showMessage({
        message: "OTP has been sent to your phone",
        type: "success",
        icon: "success",
        duration: 3000,
      });
    }
  };

  return (
    <View style={styles.pagelayout}>
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <SafeAreaView style={styles.screen}>
          <View style={styles.headingContainer}>
            <Text style={styles.heading}> Mobile Verification </Text>
            <View style={styles.OtpIcon}>
              <Image
                style={styles.iconImg}
                source={require("../assets/Otpicon.png")}
              />
            </View>
            <Text style={styles.subHeading}> Enter 4 digit OTP sent to </Text>
            <Text style={styles.numbs}>
              {mobile}
              {OtpStatus()}{" "}
            </Text>
          </View>

          <View style={styles.otpContainer}>
            <View>
              <TextInput
                ref={(input) => (textInput = input)}
                onChangeText={onChangeText}
                style={styles.OTPbox}
                value={otpInput}
                maxLength={lengthInput}
                returnKeyType="done"
                keyboardType="numeric"
              />
              <TouchableWithoutFeedback onPress={() => textInput.focus()}>
                <View style={styles.containerInput}>
                  <View
                    style={[
                      styles.cellView,
                      {
                        borderBottomColor:
                          otpInput.length == 0 ? "#1493e3" : "#9a9a9a",
                      },
                    ]}
                  >
                    <Text
                      style={styles.cellText}
                      onPress={() => textInput.focus()}
                    >
                      {otpInput && otpInput.length > 0
                        ? otpInput[0]
                        : ""}
                    </Text>
                  </View>

                  <View
                    style={[
                      styles.cellView,
                      {
                        borderBottomColor:
                          otpInput.length == 1 ? "#1493e3" : "#9a9a9a",
                      },
                    ]}
                  >
                    <Text
                      style={styles.cellText}
                      onPress={() => textInput.focus()}
                    >
                      {otpInput && otpInput.length > 0
                        ? otpInput[1]
                        : ""}
                    </Text>
                  </View>

                  <View
                    style={[
                      styles.cellView,
                      {
                        borderBottomColor:
                          otpInput.length == 2 ? "#1493e3" : "#9a9a9a",
                      },
                    ]}
                  >
                    <Text
                      style={styles.cellText}
                      onPress={() => textInput.focus()}
                    >
                      {otpInput && otpInput.length > 0
                        ? otpInput[2]
                        : ""}
                    </Text>
                  </View>

                  <View
                    style={[
                      styles.cellView,
                      {
                        borderBottomColor:
                          otpInput.length == 3 ? "#1493e3" : "#9a9a9a",
                      },
                    ]}
                  >
                    <Text
                      style={styles.cellText}
                      onPress={() => textInput.focus()}
                    >
                      {otpInput && otpInput.length > 0
                        ? otpInput[3]
                        : ""}
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>

            <OtpTimer trigger={refresh} mobile={mobile} />

            <TouchableOpacity onPress={fetchCopiedText}><Text style={{padding:10}}>Hit Me</Text></TouchableOpacity>
          </View>
        </SafeAreaView>
      </TouchableWithoutFeedback>
      <FlashMessage position="top" />
    </View>
  );
};

export default OTPScreen;

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  pagelayout: {
    backgroundColor: "white",
    flex: 1,
  },
  otpContainer: {
    flex: 1,
    alignItems: "center",
  },
  heading: {
    fontSize: 25,
    fontWeight: "500",
    marginTop: 50,
    marginBottom: 20,
  },
  OtpIcon: {
    height: 110,
    width: 110,
  },
  OTPbox: {
    width: 0,
    height: 0,
    padding: 10,
    marginLeft: 20,
  },
  containerInput: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  subHeading: {
    color: "#979799",
    fontSize: 14,
    fontWeight: "500",
    padding: 5,
    marginTop: 10,
  },
  headingContainer: {
    alignItems: "center",
  },
  iconImg: {
    height: 100,
    width: 100,
    resizeMode: "contain",
  },
  cellView: {
    //paddingVertical: 11,
    width: 40,
    margin: 8,
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderBottomWidth: 3,
  },
  cellText: {
    textAlign: "center",
    fontSize: 40,
  },
});
