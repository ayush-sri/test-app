import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import { WebView } from 'react-native-webview';


const YoutubePlayer = () => {
    const initialHTMLContent = `
            <!DOCTYPE html>
            <html>
            <body>

            <iframe width="800" height="600" src="https://www.youtube.com/embed/tgbNymZ7vqY?playlist=tgbNymZ7vqY&loop=1">
            </iframe>

            </body>
            </html>`;
    return (
        <WebView
        style={styles.container}
        originWhitelist={['*']}
        javaScriptEnabled={true}
        domStorageEnabled={true}
        source={{
          html: initialHTMLContent,
        }}
      />
    )}

export default YoutubePlayer

const styles = StyleSheet.create({
    container:{
        justifyContent:'center'
    }
})
