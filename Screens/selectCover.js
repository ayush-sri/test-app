import React, { useState, useEffect } from 'react';
import { StyleSheet, Image, Pressable, Text, View,ActivityIndicator, ScrollView, FlatList, TouchableOpacity } from 'react-native'
import FlashMessage from "react-native-flash-message";
import { showMessage } from "react-native-flash-message";
import { MaterialCommunityIcons, Entypo } from '@expo/vector-icons';
import { Video } from 'expo-av';

const selectCover = () => {

    const [loading, setLoading] = useState();
    const [taskItems, setTaskItems] = useState();
    const [highlightIndex, setHighlightIndex] = useState(null);
    const [previewItem, setPreviewItem] = useState();

    useEffect(() => {

        setLoading(true)
        fetch('https://www.rozgaarindia.com/bridge/portfolioWorkImages', {
            method: 'POST',
            timeout: 10000,
            headers: {
                Accept: 'application/json',
                'Content-Type': 'application/json',
                'Auth-Token': 'Ezxe01MBXU2werWrW2Wi342ASDADAShyIIoKvmYI'
            },
            body: JSON.stringify({
                "post_token": '162442856388',
                "type": "get",

            })
        })
            .then((response) => response.json())
            //.then((json) => console.log(json))
            .then((json) => {
                if (json.status === 'success') {
                    setTaskItems(json.data.portfolio_images)
                }
            })
            .catch((error) => (error))
            .finally(() => setLoading(false));

    }, []);

    const PreviewItemFunction = () => {
        if(highlightIndex == null)
            return(
            <Image source={require("../assets/no-cover.jpg")} resizeMode='contain' style={styles.coverPreview} />
            )
        else{
        switch (previewItem.type) {
            case 'img':
                return (
                        <Image source={{ uri: previewItem.uri }}  style={styles.coverPreview} />
                )
                break;
            case 'others':
                return (
                        <Image source={require("../assets/no-preview-available.png")} resizeMode='contain' style={styles.coverPreview} />
                )
                break;
            case 'av':
                return (
                        <Video source={{ uri: previewItem.uri }} isPlaying={false} resizeMode={Video.RESIZE_MODE_COVER}
                            useNativeControls style={styles.coverPreview} />
                )
                break;
            default:
                break;
        }
    }
    }

    const FlatlistRender = (index, pathURI, type) => {
        switch (type) {
            case 'img':
                return (
                    <Pressable
                        onPress={() => { setHighlightIndex(index);setPreviewItem({uri:pathURI,type:type}) }}
                        style={highlightIndex == index ? styles.itemSelected : styles.itemNonSelected}>
                        <Image source={{ uri: pathURI }} style={styles.items} />
                    </Pressable>

                )
                break;
            case 'others':
                return (
                    <Pressable
                        onPress={() => { setHighlightIndex(index);setPreviewItem({uri:pathURI,type:type} )}}
                        style={highlightIndex == index ? styles.itemSelected : styles.itemNonSelected}>
                        <Image source={require("../assets/no-preview-available.png")} resizeMode='contain' style={styles.items} />
                    </Pressable>
                )
                break;
            case 'av':
                return (
                    <Pressable
                        onPress={() => { setHighlightIndex(index);setPreviewItem({uri:pathURI,type:type}) }}
                        style={highlightIndex == index ? styles.itemSelected : styles.itemNonSelected}>
                        <Video source={{ uri: pathURI }} isPlaying={false} resizeMode={Video.RESIZE_MODE_COVER}
                            useNativeControls style={styles.items} />
                    </Pressable>
                )
                break;
            default:
                break;
        }
    }

    return (
        <View style={styles.mainContainer}>
            <View style={{ flexDirection: 'row' }}>
                <View style={styles.topProgressBarLeft} />
                <View style={styles.topProgressBarRight} />
            </View>
            <Text style={styles.titleText}>Select cover picture or Video</Text>
            
            <PreviewItemFunction />
            {loading ? 
        (<ActivityIndicator size="large" color="#1493e3" style={styles.activityIndicator} />)
          :
            (<ScrollView>
                <FlatList data={taskItems}
                    contentContainerStyle={styles.itemsWrapper}
                    horizontal={false}
                    numColumns={3}
                    scrollEnabled={false}
                    keyExtractor={item => item.keyIndex}
                    renderItem={({ item, index }) => (
                        <View>
                            {FlatlistRender(index, item.portfoli_img_url, item.portfoli_img_type)}
                        </View>
                    )} />
            </ScrollView>)}
            <TouchableOpacity style={styles.cnfrmBtn} >
                <Text style={styles.cnfrmBtnText}>Confirm</Text>
            </TouchableOpacity>
            <FlashMessage position="top" />
        </View>
    )
}

export default selectCover

const styles = StyleSheet.create({
    mainContainer: {
        flex: 1,
        backgroundColor: '#FFFFFF',
    },
    topProgressBarLeft: {
        width: '80%',
        borderWidth: 5,
        backgroundColor: '#007f00',
        borderColor: '#007f00',
        margin: 0,
    },
    topProgressBarRight: {
        width: '20%',
        borderWidth: 5,
        backgroundColor: '#54e599',
        borderColor: '#54e599',
        margin: 0,
    },
    titleText: {
        paddingTop: 15,
        paddingStart: 20,
        paddingBottom: 5,
        fontSize: 20,
        fontWeight: 'bold',
    },
    coverPreview: {
        width: 360,
        alignSelf: 'center',
        height: 180,
        borderRadius: 3,
        margin: 10,
        borderWidth:2,
        borderColor: '#98a8ab',
    },
    itemsWrapper: {
        alignSelf: 'center',
        paddingBottom: 10,
    },
    items: {
       // justifyContent: 'space-evenly',
        flexDirection: 'row',
        width: 100,
        alignSelf: 'center',
        height: 100,
        borderRadius: 7
    },
    itemSelected: {
        borderColor: '#69adf4',
        borderWidth: 4,
        margin: 8,
        borderRadius: 12,
        alignSelf: 'center',
    },
    itemNonSelected: {
        borderColor: '#ffffff',
        borderWidth: 4,
        margin: 8,
        borderRadius: 12,
        alignSelf: 'center',
    },
    activityIndicator:{
        flex:1
    },
    cnfrmBtn: {
        borderRadius: 5,
        backgroundColor: '#007F00',
        width: '80%',
        justifyContent: 'flex-end',
        alignItems: 'center',
        alignSelf: 'center',
        margin: 5,
        padding: 12
    },
    cnfrmBtnText: {
        fontSize: 20,
        color: '#fff'
    }
})
