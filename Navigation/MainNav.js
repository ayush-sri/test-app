import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import HomeScreen from './AppNavigation';
import { MaterialIcons,FontAwesome,FontAwesome5 } from '@expo/vector-icons'; 
import { View, Text } from 'react-native'

import { createStackNavigator } from '@react-navigation/stack';

import Home from '../Screens/Home';
import uploadFiles from '../Screens/uploadFiles';
import selectCover from '../Screens/selectCover';
import localNotification from '../Screens/localNotification';
import YoutubePlayer from '../Screens/YoutubePlayer';
import testScreen from '../Screens/testScreen'
import MediaGallery from '../Components/MediaGallery';
import OTPScreen from '../Screens/OTPScreen'

const Stack = createStackNavigator();
export const ScreenNavigation =()=>{
  return(
    
                  <Stack.Navigator >
                  <Stack.Screen name='Home' component={Tabs} />
                  <Stack.Screen name='uploadFiles' component={uploadFiles} options={{
                    title: 'Work Samples',
                    headerTitleStyle: {
                      fontWeight: 'bold',
                    },
                  }} />
                  <Stack.Screen name='selectCover' component={selectCover} options={{
                    title: 'Cover picture and video',
                    headerTitleStyle: {
                      fontWeight: 'bold',
                    },
                  }} />
                  <Stack.Screen name='localNotification' component={localNotification} options={{
                    title: 'Notification',
                    headerTitleStyle: {
                      fontWeight: 'bold',
                    },
                  }} />
                  <Stack.Screen name='YoutubePlayer' component={YoutubePlayer} options={{
                    title: 'Play Youtube Video',
                    headerTitleStyle: {
                      fontWeight: 'bold',
                    },
                  }} />
                  <Stack.Screen name='testScreen' component={testScreen} options={{
                    title: 'Test Screen',
                    headerTitleStyle: {
                      fontWeight: 'bold',
                    },
                  }} />
                 
                  <Stack.Screen name='OTPScreen' component={OTPScreen} options={{
                    title: 'OTP Screen',
                    headerTitleStyle: {
                      fontWeight: 'bold',
                    },
                  }} />
                  
                  <Stack.Screen name='MediaGallery' component={MediaGallery} options={{ headerShown: false}}/>
                  </Stack.Navigator>
             
  );
}


const Tab = createBottomTabNavigator();
export const Tabs = () => {
  return (
    <Tab.Navigator tabBarOptions={{
      activeColor: '#3dadd4',
      inactiveTintColor: '#808080',
    }}>
      <Tab.Screen name="Home" component={Home} options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="home" color={ color } size={26} />
          ),
        }}/>
      <Tab.Screen name="Chat" component={HomeScreen} options={{
          tabBarLabel: 'Chat',
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="chat" color={ color } size={26} />
          ),
        }}/>
      <Tab.Screen name="My Work" component={HomeScreen} options={{
          tabBarLabel: 'My Work',
          tabBarIcon: ({ color }) => (
            <MaterialIcons name="work" color={ color } size={26} />
          ),
        }}/>
      <Tab.Screen name="My Sales" component={HomeScreen} options={{
          tabBarLabel: 'My Sales',
          tabBarIcon: ({ color }) => (
            <FontAwesome5 name="clipboard-list" size={24} color={ color } />
          ),
        }}/>
      <Tab.Screen name="Revenue" component={HomeScreen} options={{
          tabBarLabel: 'Revenue',
          tabBarIcon: ({ color }) => (
            <FontAwesome name="rupee" color={ color } size={24} />
          ),
        }}/>
    </Tab.Navigator>
  );
}
export default Tabs;