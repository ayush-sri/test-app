import React from 'react'
import { View, Text } from 'react-native'
import { NavigationContainer } from '@react-navigation/native';


import {ScreenNavigation} from './MainNav'


const AppNavigation = () => {
  return (      
  
  <NavigationContainer>

<ScreenNavigation/>
  </NavigationContainer>
 
    )
}

export default AppNavigation
