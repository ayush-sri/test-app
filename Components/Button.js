import React from 'react'
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native'
import { useFonts } from 'expo-font';

const Button = (props) => {

    const [loaded] = useFonts({
        'Raleway-Bold': require('../assets/Fonts/Raleway-Bold.ttf'),
      });
      
      if (!loaded) {
        return null;
      }

    return (
        <View style={styles.container}>
            <View style={styles.touchable}>
                <TouchableOpacity
                    onPress={props.onPress}
                    style={styles.btn_Button}>
                    <Text style={styles.btn_Text}>{props.text}</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}

export default Button

const styles = StyleSheet.create({
    btn_Button: {
        backgroundColor: '#1493e3',
        borderRadius: 10,
        padding: 12,
        width: '60%',
        alignItems: 'center'
    },
    touchable: {
        alignItems: 'center',
    },
    btn_Text: {
        fontSize: 16,
        color: '#fff',
        fontFamily:'Raleway-Bold'
    }
})

