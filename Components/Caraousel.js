import React, { useEffect, useState } from "react";
import {
  View,
  Dimensions,
  FlatList,
  StyleSheet,
  Image,
  Animated,
  Text,
} from "react-native";
import GestureHandler, {
  PinchGestureHandler,
  State,
} from "react-native-gesture-handler";
import Video from "expo-av";
import PinchToZoom from "./PinchToZoom";

const width = Dimensions.get("window").width;
const height = Dimensions.get("window").height;

const Caraousel = (props) => {
  const [workItems, setWorkItems] = useState(props.data);
  const [index, setIndex] = useState(props.index);
  
  let scale = new Animated.Value(1);

  const onZoomEvent = Animated.event(
    [
      {
        nativeEvent: { scale: scale }
      }
    ],
    {
      useNativeDriver: true
    }
  )

  const onZoomStateChange = event => {
      if (event.nativeEvent.oldState === State.ACTIVE) {
        Animated.spring(scale, {
          toValue: 1,
          useNativeDriver: true
        }).start()
      }
    }

  const indexIncrease = (count) => {
    let newCount = count + 1;
    let totalImages = workItems.length;
    return (
      <Text>
        {newCount}/ {totalImages}{" "}
      </Text>
    );
  };

  const ViewSwitch = (count, path, type) => {
    switch (type) {
      case "img": {
        return (
          <View style={styles.cardView}>
            <View style={styles.image}>
            <PinchToZoom uri={path} />
            </View>
            {/* <Text>{indexIncrease(count)} </Text> */}
          </View>
        );
      }
      case "av": {
        return (
          <View style={styles.cardView}>
            <Video
              source={{ uri: path }}
              isPlaying={false}
              resizeMode={"contain"}
              useNativeControls
              style={styles.image}
            />
          </View>
        );
      }
      default:
        break;
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <FlatList
        data={workItems}
        keyExtractor={(item) => item.keyIndex}
        horizontal
        initialScrollIndex={index}
        pagingEnabled
        scrollEnabled
        snapToAlignment="center"
        scrollEventThrottle={16}
        decelerationRate={"fast"}
        showHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <View style={{}}>
            <View>{ViewSwitch(index, item.uri, item.uri_type)}</View>
          </View>
        )}
      />
    </View>
  );
};

export default Caraousel;
const styles = StyleSheet.create({
  cardView: {
    flex: 1,
    width: width - 20,
    height: height / 2,
    backgroundColor: "white",
    margin: 10,
  },
  image: {
    width: width,
    height: width,
  },
});
