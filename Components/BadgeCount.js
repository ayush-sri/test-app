import React, { useEffect } from 'react';
import { View } from 'react-native'
import * as Notifications from 'expo-notifications';

const BadgeCount = (props) => {
    const count = props.count;
    useEffect(() => {
        const permissionResult = Notifications.requestPermissionsAsync({
            ios: {
              allowAlert: true,
              allowBadge: true,
              allowSound: true,
              allowAnnouncements: true,
            },
          });
        if (permissionResult.granted === false) {
            createAlert();
        }
        Notifications.setNotificationHandler({
            handleNotification: async () => ({
              shouldShowAlert: true,
              shouldPlaySound: true,
              shouldSetBadge: true,
            }),
          });
            Notifications.setBadgeCountAsync(count);   
      }, []);
      const open_settings = () => {
        if (Platform.OS === 'ios') {
            Linking.openURL(`app-settings:`);
          } else {
            IntentLauncher.startActivityAsync(IntentLauncher.ACTION_APPLICATION_DETAILS_SETTINGS, {
              data: `package:${Application.applicationId}`,
            });
          }
      }
    const createAlert = () =>
      Alert.alert(
        "Permission error !",
        "Please grant notification permission from app settings",
        [
          {
            text: "Cancel",
          },
          { text: "OK", onPress: () => open_settings() }
        ],
        { cancelable: false }
      );
    return (
        <View>
        </View>
    )
}

export default BadgeCount
