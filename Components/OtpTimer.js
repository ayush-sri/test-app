import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
// import ApiCatch from '../Components/ApiCatch';
import { showMessage } from "react-native-flash-message";
// import EventAction from './FirebaseAnalytics/EventAction';

const getRemaining = (time) => {
    const mins = Math.floor(time / 60)
    const secs = time - mins * 60;
    return { mins, secs };
}

const OtpTimer = props => {

    useEffect(() => {
        // EventAction({ screenName: 'OtpTimer', action: 'UseEffect' });
    }, []);

    const [remainingSecs, setremainingSecs] = useState(30);
    const { mins, secs } = getRemaining(remainingSecs);
    const [isSendDisable, setisSendDisable] = useState(true);

    useEffect(() => {
        let interval = null;
        if (remainingSecs > 0) {
            interval = setInterval(() => {
                setremainingSecs(remainingSecs => remainingSecs - 1);
            }, 1000);
        }
        else if (remainingSecs == 0) {
            setisSendDisable(false)
            clearInterval(interval);
        }
        return () => clearInterval(interval);
    }, [remainingSecs, props.trigger]);


    const handleOtpGet = () => {
        if (!isSendDisable) {
            setremainingSecs(30)
            setisSendDisable(true)
        }
    }
    const resendOtp = () => {
        try {
            fetch('https://www.rozgaarindia.com/bridge/send_otp', {
                method: 'POST',
                timeout: 10000,
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                    'Auth-Token': 'QtLRxjAC4ulU19iyX0FzxU6VB5Wwm8cBbtlVkxsF'
                },
                body: JSON.stringify({
                    "mobile": (props.mobile),
                })
            })
                .then((response) => response.json())
                .then((result) => {
                    if (result.status === "SUCCESS") {
                        showMessage({
                            message: "OTP has been sent to your phone",
                            type: 'success',
                            icon: "success",
                            duration: 3000
                        });
                    } else {
                        showMessage({
                            message: "Please check your internet",
                            description: "There is some issue is sending OTP",
                            type: 'danger',
                            icon: "danger",
                            duration: 3000
                        });
                    };
                })
                // .catch((error) => ApiCatch({ error: error, source: 'OtpScreen - send_otp' }))
        } 
        catch (error) {
            // ApiCatch({ error: error, source: 'OtpScreen - send_otp' })
        }
    }

    return (

        <View style={{ margin: 10 }}>
            <View style={{ marginTop: 0, marginBottom: 10 }}>
                <Text style={{ color: '#B2B2B2' }}>OTP will send after {`${secs}`} seconds. </Text>
            </View>
            <TouchableOpacity disabled={isSendDisable}>
                <Text disabled={isSendDisable}
                    style={{ color: isSendDisable ? '#B2B2B2' : '#1E90FF', fontSize: 15, alignSelf: "center", fontWeight: 'bold' }}
                    onPress={() => { handleOtpGet(), resendOtp() }}> Resend Code</Text>
            </TouchableOpacity>
        </View>

    );
}

export default OtpTimer