import React,{useState} from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const MediaGallery = () => {
  const [state, setstate] = useState(false)
  const Comp1 = () =>{
    return(
      <View></View>
    )
  }
  const Comp2 = () =>{
    return(
      <View style={{backgroundColor:'red'}}>
        
        <Text>Hi</Text>
        
      </View>
    )
  }
  return (
    <View style={{marginTop:30,flex:1}}>
      <TouchableOpacity onPress={()=>setstate(false)}>
      <Text>PRESS ME</Text>
      </TouchableOpacity>
      {state?<Comp1/>:<Comp2/>}
    </View>
  )
}

export default MediaGallery

const styles = StyleSheet.create({})
