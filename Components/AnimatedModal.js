import React,{useState} from 'react'
import { View,Modal, Text } from 'react-native'
import { Portal,Provider } from 'react-native-paper';
import { gestureHandlerRootHOC } from "react-native-gesture-handler";

const AnimatedModal = () => {

    const [showModal, setShowModal] = useState(false);
    const ModalInner = gestureHandlerRootHOC(function GestureExample() {
        return (
          <View>
            <PinchToZoom uri="https://res.cloudinary.com/rozgaarindia/image/upload/w_800,c_scale,q_90/v1621668316/portfolio/phpUre5gx_t4heyy.png"/>
          </View>
        );
      });
    return (
        <Provider>
      <Modal
        transparent={false}
        visible={showModal}
        onRequestClose={() => {
          setShowModal(!showModal);
        }}
      >
       <ModalInner />
       </Modal>
      </Provider>
    )
}

export default AnimatedModal
